(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
(* [@@@warning "-27-32-33-37-39"] *)

open SystemXi
open DPJS

(* This is a generator of fresh variable names. *)

let postincrement c =
  let y = !c in
  c := y + 1;
  y

let fresh_var =
  let c = ref 0 in
  fun base ->
    Printf.sprintf "%s_%d" base (postincrement c)

(* The following auxiliary functions help construct DPJS terms. *)

let (!) x =
  EVar x

let lambda x e =
  ELam (x, e)

let lambda_many xs e =
  let x = fresh_var "b" in
  lambda x (ELetTuple (xs, !x, e))

let (@) e1 e2 =
  EApp (e1, ETuple e2)

let def x e1 e2 =
  ELet (x, e1, e2)

let pushPrompt p e =
  EPushPrompt (p, e)

let pushSubCont k e =
  EPushSubCont (k, e)

let unpack x =
  let result = fresh_var "unpacked" in
  ELetTuple([result], !x, !result)

let unchoice x =
    match x with
    | Block x -> x
    | Value x -> x

let translate_statement s =
   let rec ts s =
      match s with
      | SRange (r, s) ->
              ERange (r, ts s)
      | SLetVal (x, s1, s2) ->
              def x (ts s1) (ts s2)
      | SReturn e ->
              te e
      | SIf (e, s1, s2) ->
              EIf (te e, ts s1, ts s2)
      | SPrint e ->
              EPrint (te e)
      | SLetBlock (x, b, s) ->
              def x (tb b) (ts s)
      | SHandle (b1, b2) ->
              let e1 = tb b1 in
              let e2 = tb b2 in
              (* We follow the naming convention of [test/inputs/simple.th] *)
              (* [p] is our main prompt *)
              let p = fresh_var "p" in
              (* [k] is the subcont argument *)
              let k = fresh_var "k" in
              (* [x] is the argument of inner *)
              let x = fresh_var "x" in
              (* [y] is the argument of resume *)
              let y = fresh_var "y" in
              let resume = lambda y (
                  pushPrompt !p (
                      pushSubCont !k (unpack y)
                  )
              ) in
              let block2 = e2 @ [unpack x ; resume] in
              let sub_cont = EWithSubCont (!p, (lambda k block2)) in
              let inner = lambda x sub_cont in
              def p ENewPrompt (
                  pushPrompt !p (
                      e1 @ [inner]
                  )
              )


    and te e =
      match e with
      | ExVar x ->
              !x
      | ExBool b ->
              EBool b
      | ExString s ->
              EString s
      | ExRange (r, e) ->
              ERange (r, te e)
      | ECall (b, a) ->
              tb b @ List.map ta a

    and tb b =
      match b with
      | BlockVar x ->
              !x
      | BlockDef (fs, s) ->
              lambda_many (List.map unchoice fs) (ts s)
      | BlockRange (r, b) ->
              ERange (r, tb b)

    and ta a =
        match a with
        | Value e -> te e
        | Block b -> tb b

   in ts s

