(* This file implements the "subcontinuation" monad, also known as the delimited
   continuation monad, which is described in the paper "A Monadic Framework for
   Delimited Continuations", by Dybvig, Peyton Jones, and Sabry (2007). Our
   implementation is inspired by Dybvig et al.'s first implementation, which is
   given in Sections 7.1, 7.2 and 7.3 of the paper, but improves on it in two
   ways, indicated in the comments below. *)

open Tag

(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
(* [@@@warning "-27-32-37-39"] *)

(* -------------------------------------------------------------------------- *)

(* A prompt is a tag. *)

type 'a prompt =
  'a tag

(* If desired, we could reveal the existence of an equality test on prompts,
   implemented by [Tag.equal]. This might be useful in some applications. *)

(* -------------------------------------------------------------------------- *)

(* The following four type definitions are mutually recursive. *)

(* We use a CPS monad: a computation is a function of a context to an answer.
   We improve upon Dybvig, Peyton Jones, and Sabry's Haskell implementation by
   enforcing the invariant that every computation is polymorphic in the type
   ['answer]. *)

(* OCaml is not System F: universal quantification is not part of the syntax
   of types. A polymorphic function type must be encapsulated inside a record
   type. See Section 5.3 of the manual, "Higher-rank polymorphic functions".
   https://caml.inria.fr/pub/docs/manual-ocaml/polymorphism.html#s:higher-rank-poly
 *)

type 'a m = {t: 'b. ('a, 'b) context -> 'b }
    (* ('a, 'a) context -> 'a *)

(* A context (also known as a delimited continuation) is not represented as a
   function; it is represented as a data structure, that is, a stack.

   When thinking of a context as a function, we prefer to write its type under
   the form [('top, 'bot) context], which can be read like the function type
   ['top -> 'bot].

   When thinking of a context as a stack, we prefer to write its type under
   the form [('bot, 'top) stack], because we write our stacks from left to
   right. The left end is the bottom; the right end is the top. Elements are
   pushed and popped at the top. *)

and ('top, 'bot) context =
  ('bot, 'top) stack

(* As in Dybvig et al.'s paper, a stack is a sequence of elements of two
   kinds: 1- ordinary frames; 2- prompts. See page 11 of the paper, where
   the syntax of stacks is E ::= [] | D : E | p : E. In our terminology,
   E is a context, D is a frame, p is a prompt. *)

(* As in Dybvig et al.'s paper, the type [('bot, 'top) stack] is
   parameterized by the type ['top] of the values that flow into
   the stack and by the type ['bot] of the values that flow out of
   the stack. For this reason, the type [stack] is a GADT. *)

(* We improve upon Dybvig et al.'s paper by imposing the invariant that a
   stack cannot contain two consecutive frames. That is, two frames must be
   separated by at least one prompt. This invariant is not visible in the
   following type definition, but is enforced in our code. Thanks to this
   invariant, it is possible to jump from one prompt to the next prompt
   in time O(1). *)

(* Inspired by module semantics page 11
 * ```
 * data Seg contseg ans a where
 *      EmptyS :: Seq contseg ans ans
 *      PushP :: Prompt ans a -> Seq contseg ans a -> Seq contseg ans a
 *      PushSeg :: contseg ans a b -> Seq contseg ans b -> Seg contseg ans a
 * ```
 * A difference is that we don't need the contseg argument, which is just
 * the Frame constructor.
 * *)
and ('bot, 'top) stack = (* data Seq contseg ans a where *)
    | SEmpty : ('bot, 'bot) stack
    | SPrompt : 'top prompt * ('bot, 'top) stack -> ('bot, 'top) stack
    | SFrame : ('mid, 'top) frame * ('bot, 'mid) stack -> ('bot, 'top) stack

(* A frame of type [('bot, 'top) frame] is an ordinary evaluation
   context, that is, an evaluation context that does not contain
   any prompt. It can be represented as an effectful function of
   type ['top] to ['bot]. *)

and ('bot, 'top) frame = 'top -> 'bot m
(* -------------------------------------------------------------------------- *)

(* The smart constructor [cons] enforces the invariant that a stack cannot
   contain two consecutive [SFrame] constructors. It exploits the auxiliary
   function [compose], which composes two frames into a single frame. As the
   definition of [compose] involves [bind], and as [bind] itself depends on
   [cons], the following three definitions must be mutually recursive. *)

let rec bind
: type a b .
  a m -> (a -> b m) -> b m
 = fun g f -> {t = fun s -> g.t (cons s f) }

and cons
: type bot mid top .
  (bot, mid) stack -> (mid, top) frame -> (bot, top) stack
= function
    | SFrame(f1, s1) -> (fun f2 -> SFrame(compose f1 f2, s1))
    | s -> (fun f -> SFrame(f, s))

and compose
: type bot mid top .
  (bot, mid) frame -> (mid, top) frame -> (bot, top) frame
= fun f1 f2 x -> bind (f2 x (*: mid m *)) f1

(* Sugar. *)

let (let*) =
  bind

(* -------------------------------------------------------------------------- *)

(* [apply] applies a context to a value, producing an answer. *)

(* [apply] can be viewed as the function that interprets a defunctionalized
   continuation of type [('a, 'b) context] as a function of type ['a -> 'b]. *)

(* [apply] can also be understood as the operation of filling an evaluation
   context with a value, and continuing execution from there. *)

let rec apply
: type a b . (a, b) context -> a -> b
= function
 | SEmpty -> (fun x -> x)
 | SFrame(f, s) -> (fun x -> let g = f x in g.t s)
 | SPrompt(_, s) -> apply s

(* This is the standard definition of [return] in a CPS monad. *)

let return
: type a . a -> a m
= fun x -> {t = fun s -> apply s x}

(* This is the standard definition of [run] in a CPS monad. *)

let run
: type a . a m -> a
= fun f -> f.t SEmpty

(* -------------------------------------------------------------------------- *)

(* The definitions of [new_prompt] and [push_prompt] are straightforward. *)

let new_prompt
: type a. a prompt m
= {t = fun s -> let p = new_tag() in apply s p }

(**The computation [push_prompt p f] first pushes the prompt [p] onto the
   stack; then, it executes the computation [f], yielding a result [v];
   finally, it pops [p] off the stack and returns [v]. *)
let push_prompt
: type a. a prompt -> a m -> a m
(* TODO finally, it pops [p] off the stack and returns [v].*)
= fun (p: a prompt) (f: a m) -> (* bind {t =
    fun s -> f.t (SPrompt(p, s))
} (fun v -> return v)
*) {t = fun s -> f.t (SPrompt(p, s))}

(* -------------------------------------------------------------------------- *)

(* [append stack1 stack2] is the concatenation of the stacks [stack1]
   and [stack2]. *)

(* The smart constructor [cons] is used (where appropriate) so as to
   respect the invariant that a stack cannot contain two consecutive
   [SFrame] constructors. *)

let rec append
: type bot mid top .
  (bot, mid) stack -> (mid, top) stack -> (bot, top) stack
= fun s1 -> function
  | SEmpty -> s1
  | SFrame(f, SEmpty) -> cons s1 f
  | SFrame(f, s3) -> SFrame(f, append s1 s3)
  | SPrompt(p, s3) -> SPrompt(p, append s1 s3)

(* [push_subcont] performs a stack concatenation operation. *)

let push_subcont
: type a b . (a, b) context -> a m -> b m
= fun s1 g -> {t = fun s2 -> g.t (append s2 s1)}

(* [apply_subcont] is a simple combination of [push_subcont] and [return]. *)

let apply_subcont
: type a b . (a, b) context -> a -> b m
= fun c x -> push_subcont c (return x)

(* -------------------------------------------------------------------------- *)

(* [split tag stack] searches the stack [stack] for the tag [tag]. If it is
   found, then [stack] is split into three parts: a partial stack [back], the
   prompt [p], and a partial stack [front]. The pair [(back, front)] is
   returned. If the tag [p] is not found, then [PromptNotFound] is raised. *)

(* Because the stack cannot contain two consecutive [SFrame] constructors,
   we are able to jump from one prompt to the next prompt in time O(1).
   Thus, the time complexity of [split p] is linear in the number of
   prompts that have been pushed onto the stack after [p] was pushed. *)

exception PromptNotFound

let rec split
: type bot mid top .
  mid tag ->
  (bot, top) stack ->
  (bot, mid) stack * (mid, top) stack
= fun t s -> match s with
    | SEmpty -> raise PromptNotFound
    | SFrame(f, s1) -> let (l, r) = split t s1 in (l, cons r f)
    | SPrompt(u, s1) -> match Tag.equal u t with
        | Some Eq.Eq -> (s1, SEmpty)
        | None -> let (l, r) = split t s1 in (l, SPrompt(u, r))

(* [with_subcont] performs a stack splitting operation. *)

let with_subcont
: type a b . b prompt -> ((b, a) stack -> b m) -> a m
= fun p f -> {t= fun k ->
    let (k', subk) = split p k in
    let g = f subk in
    g.t k'
}
