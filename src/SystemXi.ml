(* This is the abstract syntax of System Xi. *)

(* A value variable is an identifier that begins with a lowercase letter. *)

type vvar =
  string

(* A block variable is an identifier that begins with an uppercase letter. *)

type bvar =
  string

(* The type [choice] is used in a few places to indicate a choice between
   the syntactic category of values and the syntactic category of blocks. *)

type ('v, 'b) choice =
  | Value of 'v
  | Block of 'b

(* A range is a pair of positions in a source file. It is used when
   reporting a type error or a runtime error. *)

type range =
  Lexing.position * Lexing.position

(* The syntactic category of statements is roughly as in Figure 3a of
   Brachthäuser et al.'s paper. One small difference is that a function
   call is viewed here as an expression; this is more expressive. *)

type statement =
  | SRange of range * statement
  | SLetVal of vvar * statement * statement
  | SReturn of expression
  | SLetBlock of bvar * block * statement
  | SHandle of block * block
  | SPrint of expression
  | SIf of expression * statement * statement

(* The syntactic category of expressions. *)

and expression =
  | ExRange of range * expression
  | ExVar of vvar
  | ExBool of bool
  | ExString of string
  | ECall of block * actuals

(* The syntactic category of blocks. *)

and block =
  | BlockRange of range * block
  | BlockVar of bvar
  | BlockDef of formals * statement

(* Actual arguments appear in function calls. An actual argument is
   either an expression or a block. *)

and actuals =
  actual list

and actual =
  (expression, block) choice

(* Formal arguments appear in function definitions. A formal argument
   is either a value variable or a block variable. *)

and formals =
  formal list

and formal =
  (vvar, bvar) choice


open Printf

let rec printxis = function
    | SRange(_, s) -> printxis s
    | SLetVal(v, s1, s2) -> sprintf "Let %s = %s in %s" v (printxis s1) (printxis s2)
    | SIf(e1, s1, s2) -> sprintf "If %s then %s else %s" (printxie e1) (printxis s1) (printxis s2)
    | SHandle(b1, b2) -> sprintf "Handle %s with %s" (printxib b1) (printxib b2)
    | SLetBlock(v, b, s) -> sprintf "LetB %s = %s in %s" v (printxib b) (printxis s)
    | SPrint(e) -> sprintf "Print (%s)" (printxie e)
    | SReturn(e) -> sprintf "Return (%s)" (printxie e)

and printxie = function
    | ExBool(true) -> "True"
    | ExBool(false) -> "False"
    | ExString(s) -> sprintf "\"%s\"" (String.escaped s)
    | ExRange(_, e) -> printxie e
    | ExVar(v) -> sprintf "!%s" v
    | ECall(b, a) -> sprintf "%s (%s)" (printxib b) (String.concat ", " (List.map printxia a))

and printxia = function
    | Value(e) -> printxie e
    | Block(b) -> printxib b

and printxib = function
    | BlockRange(_, b) -> printxib b
    | BlockDef(f, s) -> sprintf "(%s) {\n%s\n}" (String.concat ", " (List.map printxif f)) (printxis s)
    | BlockVar(v) -> sprintf "!%s" v

and printxif = function
    | Value(v) -> v
    | Block(b) -> b
