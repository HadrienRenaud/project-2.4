(* This is the abstract syntax of the calculus DPJS. *)

type var =
  string

(* A range is a pair of positions in a source file. It is used when
   reporting a type error or a runtime error. *)

type range =
  Lexing.position * Lexing.position

type expr =
  | ERange of range * expr
  | EVar of var
  | ELam of var * expr
  | EApp of expr * expr
  | ELet of var * expr * expr
  | ETuple of expr list
  | ELetTuple of var list * expr * expr
  | ENewPrompt
  | EPushPrompt of expr * expr
  | EWithSubCont of expr * expr
  | EPushSubCont of expr * expr
  | EBool of bool
  | EIf of expr * expr * expr
  | EString of string
  | EPrint of expr

open Printf

let rec printe = function
 | ERange(_, e) -> printe e
  | EVar(v) -> sprintf "%s" v
  | ELam(v, e) -> sprintf "(fun %s -> %s)" v (printe e)
  | EApp(e1, e2) -> sprintf "%s %s" (printe e1) (printe e2)
  | ELet(v, e1, e2) -> sprintf "let %s = %s in %s" v (printe e1) (printe e2)
  | ETuple(l) -> sprintf "(%s)" (String.concat ", " (List.map printe l))
  | ELetTuple(v, e1, e2) -> sprintf "let (%s) = %s in %s" (String.concat ", " v) (printe e1) (printe e2)
  | ENewPrompt -> "NewPrompt()"
  | EPushPrompt(e1, e2) -> sprintf "PushPrompt(%s, %s)" (printe e1) (printe e2)
  | EWithSubCont(e1, e2) -> sprintf "WithSubCont(%s, %s)" (printe e1) (printe e2)
  | EPushSubCont(e1, e2) -> sprintf "PushSubCont(%s, %s)" (printe e1) (printe e2)
  | EBool(true) -> "True"
  | EBool(false) -> "False"
  | EIf(e1, e2, e3) -> sprintf "if %s then %s else %s" (printe e1) (printe e2) (printe e3)
  | EString(s) -> sprintf "\"%s\"" (String.escaped s)
  | EPrint(e) -> sprintf "Print(%s)" (printe e)

