# Project MPRI 2.4

**Goal**: The purpose of this programming project is to implement a type checker and an interpreter for a tiny programming language equipped with a simple form of effect handlers.

_cf [`sujet.pdf`](./sujet.pdf) for more info_



